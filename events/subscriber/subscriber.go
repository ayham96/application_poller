package subscriber

import (
	"encoding/json"

	"github.com/prometheus/common/log"
	"github.com/streadway/amqp"
	"gitlab.com/ayham96/application_poller/events"
)

type SubscriberConfig struct {
	ExchangeName          string
	QueueName             string
	MaxConcurrentHandlers int
}

type Subscriber struct {
	ch      *amqp.Channel
	q       *amqp.Queue
	config  *SubscriberConfig
	handler func(*events.ApplicationCreated) error
}

func New(ch *amqp.Channel, applicationCreatedHandler func(*events.ApplicationCreated) error, config *SubscriberConfig) (*Subscriber, error) {
	// Declare exchange
	err := ch.ExchangeDeclare(
		config.ExchangeName, // name
		"topic",             // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)

	if err != nil {
		return nil, err
	}

	// Declare and bind queue
	q, err := ch.QueueDeclare(
		config.QueueName, // name
		true,             // durable
		false,            // delete when unused
		false,            // exclusive
		false,            // no-wait
		nil,              // arguments
	)
	if err != nil {
		return nil, err
	}
	err = ch.QueueBind(
		config.QueueName,                    // queue name
		events.RoutingKeyApplicationCreated, // routing key
		config.ExchangeName,                 // exchange
		false,
		nil)
	if err != nil {
		return nil, err
	}

	return &Subscriber{
		ch:      ch,
		q:       &q,
		handler: applicationCreatedHandler,
		config:  config,
	}, nil
}

func (sub *Subscriber) Run() error {
	// Register consumer
	msgs, err := sub.ch.Consume(
		sub.config.QueueName, // queue
		"",                   // consumer
		false,                // auto ack
		false,                // exclusive
		false,                // no local
		false,                // no wait
		nil,                  // args
	)
	if err != nil {
		return err
	}

	for i := 0; i < sub.config.MaxConcurrentHandlers; i++ {
		go sub.consume(msgs)
	}

	return nil
}

func (sub *Subscriber) consume(msgs <-chan amqp.Delivery) {
	for msg := range msgs {
		event := events.ApplicationCreated{}
		err := json.Unmarshal(msg.Body, &event)
		if err != nil {
			log.Warnf("failed to unmarshal msg: %s", err)
			msg.Ack(false)
			continue
		}

		err = sub.handler(&event)
		if err != nil {
			log.Warnf("failed to handle application created event: %s", err)
			msg.Ack(false)
			continue
		}

		msg.Ack(true)
	}
}
