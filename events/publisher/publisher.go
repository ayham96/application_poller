package publisher

import (
	"encoding/json"

	"github.com/streadway/amqp"
	"gitlab.com/ayham96/application_poller/events"
)

const routingKeyApplicationStatusUpdated = "application.status_updated"

type Publisher struct {
	ch           *amqp.Channel
	exchangeName string
}

func New(ch *amqp.Channel, exchangeName string) (*Publisher, error) {
	err := ch.ExchangeDeclare(
		exchangeName, // name
		"topic",      // type
		true,         // durable
		false,        // auto-deleted
		false,        // internal
		false,        // no-wait
		nil,          // arguments
	)

	if err != nil {
		return nil, err
	}

	return &Publisher{
		ch:           ch,
		exchangeName: exchangeName,
	}, nil
}

func (pub *Publisher) ApplicationStatusUpdated(event *events.ApplicationStatusUpdated) error {
	payloadJSON, err := json.Marshal(event)
	if err != nil {
		return err
	}

	err = pub.ch.Publish(
		pub.exchangeName,                   // exchange
		routingKeyApplicationStatusUpdated, // routing key
		false,                              // mandatory
		false,                              // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        payloadJSON,
		})

	return err
}
