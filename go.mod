module gitlab.com/ayham96/application_poller

go 1.14

require (
	github.com/Netflix/go-env v0.0.0-20201103003909-014a952cefe2
	github.com/prometheus/common v0.15.0
	github.com/streadway/amqp v1.0.0
)
