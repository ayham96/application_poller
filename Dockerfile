# Builder
FROM golang:1.14 AS build

WORKDIR /app
COPY . .

RUN go build -mod vendor -o service main.go

# Runner
FROM golang:1.14

COPY --from=build /app/service /app/service

CMD ["/app/service"]
