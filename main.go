package main

import (
	"fmt"
	"log"
	"time"

	"github.com/Netflix/go-env"
	"github.com/streadway/amqp"
	"gitlab.com/ayham96/application_poller/events/publisher"
	"gitlab.com/ayham96/application_poller/events/subscriber"
	"gitlab.com/ayham96/application_poller/job/jobsclient"
	"gitlab.com/ayham96/application_poller/job/jobservice"
)

type Config struct {
	AMQPConURL string `env:"AMQP_CON_URL,default=amqp://rabbit_user:rabbit_pass@localhost:5673/"`

	ApplicationsExchangeName   string `env:"APPLICATIONS_EXCHANGE_NAME,default=loan_applications_exchange"`
	ApplicationPollerQueueName string `env:"APPLICATION_POLLER_QUEUE_NAME,default=application_poller"`
	JobsAPIBaseURL             string `env:"JOBS_API_BASE_URL,default=http://localhost:8000/api/jobs"`
}

func main() {
	config := parseConfig()
	amqpCon, err := amqp.Dial(config.AMQPConURL)
	failOnErr(err, "failed to connect to RabbitMQ")
	defer amqpCon.Close()

	pubCh, err := amqpCon.Channel()
	failOnErr(err, "failed to create publisher channel")
	pub, err := publisher.New(pubCh, config.ApplicationsExchangeName)
	failOnErr(err, "failed to create publisher")

	jobSvc := jobservice.New(
		jobsclient.New(config.JobsAPIBaseURL),
		pub,
		&jobservice.JobServiceConfig{
			JobPollMaxTries: 25,
			JobPollInterval: 1 * time.Second,
		},
	)

	subCh, err := amqpCon.Channel()
	defer subCh.Close()
	sub, err := subscriber.New(
		subCh,
		jobSvc.HandleApplicationCreated,
		&subscriber.SubscriberConfig{
			ExchangeName:          config.ApplicationsExchangeName,
			QueueName:             config.ApplicationPollerQueueName,
			MaxConcurrentHandlers: 10,
		},
	)
	failOnErr(err, "failed to create subscriber")

	forever := make(chan bool)
	err = sub.Run()
	failOnErr(err, "failed to start subscriber")

	<-forever
}

func parseConfig() *Config {
	var config Config
	_, err := env.UnmarshalFromEnviron(&config)
	failOnErr(err, "failed to read config")
	return &config
}

func failOnErr(err error, msg string) {
	if err != nil {
		log.Fatalln(fmt.Sprintf("%s : %v", msg, err))
	}
}
