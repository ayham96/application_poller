package jobsclient

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type JobResponse struct {
	ID            string `json:"id"`
	ApplicationID string `json:"application_id"`
	Status        string `json:"status"`
}

type JobsClient struct {
	baseURL string
}

func New(baseURL string) *JobsClient {
	return &JobsClient{
		baseURL: baseURL,
	}
}

func (client *JobsClient) GetByApplicationID(id string) (*JobResponse, error) {
	resp, err := http.Get(fmt.Sprintf("%s?application_id=%s", client.baseURL, id))
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf(
			"failed to create application got http status %d expected %d",
			resp.StatusCode,
			http.StatusOK,
		)
	}

	var jobResp JobResponse
	err = json.Unmarshal(body, &jobResp)
	if err != nil {
		return nil, fmt.Errorf("failed to read request body: %s", err)
	}

	return &jobResp, err
}
