package jobservice

import (
	"errors"
	"testing"
	"time"

	"gitlab.com/ayham96/application_poller/events"
	"gitlab.com/ayham96/application_poller/job/jobsclient"
)

const (
	testID          = "test-id"
	jobPollMaxTries = 4
	JobPollInterval = 50 * time.Millisecond
)

type mockClient struct {
	t            *testing.T
	returnStatus string
	returnErr    error
	callCounter  int
	lastCall     time.Time
}

func (mock *mockClient) GetByApplicationID(id string) (*jobsclient.JobResponse, error) {
	if id != testID {
		mock.t.Errorf("expected id to be %s got %s", testID, id)
	}

	mock.callCounter++

	now := time.Now()
	if mock.lastCall.Add(JobPollInterval).After(now) {
		mock.t.Errorf("called faster than poll interval")
	}
	mock.lastCall = now

	status := statusPending
	if mock.callCounter > jobPollMaxTries/2 {
		status = mock.returnStatus
	}
	return &jobsclient.JobResponse{
		ApplicationID: id,
		Status:        status,
	}, mock.returnErr
}

type mockPublisher struct {
	returnErr      error
	callCounter    int
	expectedStatus string
	t              *testing.T
}

func (mock *mockPublisher) ApplicationStatusUpdated(event *events.ApplicationStatusUpdated) error {
	if event.ApplicationID != testID {
		mock.t.Errorf("expected id to be %s got %s", testID, event.ApplicationID)
	}
	if event.Status != mock.expectedStatus {
		mock.t.Errorf("expected status to be %s got %s", mock.expectedStatus, event.Status)
	}
	mock.callCounter++
	return mock.returnErr
}

func TestJobService_HandleApplicationCreated(t *testing.T) {
	config := &JobServiceConfig{
		JobPollMaxTries: jobPollMaxTries,
		JobPollInterval: JobPollInterval,
	}
	type fields struct {
		api *mockClient
		pub *mockPublisher
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "publishes completed status",
			fields: fields{
				api: &mockClient{
					t:            t,
					returnStatus: statusCompleted,
				},
				pub: &mockPublisher{
					t:              t,
					expectedStatus: statusCompleted,
				},
			},
		},
		{
			name: "publishes timeout if still pending after max tries",
			fields: fields{
				api: &mockClient{
					t:            t,
					returnStatus: statusPending,
				},
				pub: &mockPublisher{
					t:              t,
					expectedStatus: statusTimeout,
				},
			},
		},
		{
			name: "publishes timeout after max failed tries",
			fields: fields{
				api: &mockClient{
					t:         t,
					returnErr: errors.New("couldn't reach api"),
				},
				pub: &mockPublisher{
					t:              t,
					expectedStatus: statusTimeout,
				},
			},
		},
		{
			name: "return error if it fails to publish",
			fields: fields{
				api: &mockClient{
					t:            t,
					returnStatus: statusCompleted,
				},
				pub: &mockPublisher{
					t:              t,
					expectedStatus: statusCompleted,
					returnErr:      errors.New("failed to publish"),
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			svc := &JobService{
				api:    tt.fields.api,
				pub:    tt.fields.pub,
				config: config,
			}
			err := svc.HandleApplicationCreated(&events.ApplicationCreated{
				ApplicationID: testID,
			})
			if (err != nil) != tt.wantErr {
				t.Errorf("JobService.HandleApplicationCreated() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.fields.api.callCounter == 0 {
				t.Errorf("job not fetched from client")
			}
			if tt.fields.api.callCounter > jobPollMaxTries {
				t.Errorf("exceeded maximum max tries expected %d got %d", jobPollMaxTries, tt.fields.api.callCounter)
			}
			if tt.fields.pub.callCounter != 1 {
				t.Errorf("expected to publish event once got %d times", tt.fields.pub.callCounter)
			}
		})
	}
}
