package jobservice

import (
	"time"

	"github.com/prometheus/common/log"
	"gitlab.com/ayham96/application_poller/events"
	"gitlab.com/ayham96/application_poller/job/jobsclient"
)

const (
	statusPending   = "pending"
	statusCompleted = "completed"
	statusTimeout   = "timeout"
)

type JobServiceConfig struct {
	JobPollMaxTries int
	JobPollInterval time.Duration
}

type JobsClient interface {
	GetByApplicationID(id string) (*jobsclient.JobResponse, error)
}

type ApplicationStatusPublisher interface {
	ApplicationStatusUpdated(event *events.ApplicationStatusUpdated) error
}

type JobService struct {
	api    JobsClient
	pub    ApplicationStatusPublisher
	config *JobServiceConfig
}

func New(api JobsClient, pub ApplicationStatusPublisher, config *JobServiceConfig) *JobService {
	return &JobService{
		api:    api,
		pub:    pub,
		config: config,
	}
}

func (svc *JobService) HandleApplicationCreated(event *events.ApplicationCreated) error {
	jobStatus := statusPending
	for i := 0; i < svc.config.JobPollMaxTries; i++ {
		if i != 0 {
			time.Sleep(svc.config.JobPollInterval)
		}

		newStatus, err := svc.FetchApplicationStatus(event.ApplicationID)
		if err != nil {
			log.Warnf("failed to fetch application status %s", err)
			continue
		}
		jobStatus = newStatus
		if jobStatus != statusPending {
			break
		}
	}
	if jobStatus == statusPending {
		jobStatus = statusTimeout
	}

	err := svc.pub.ApplicationStatusUpdated(&events.ApplicationStatusUpdated{
		ApplicationID: event.ApplicationID,
		Status:        jobStatus,
	})
	if err != nil {
		log.Warnf("failed to publish application status %s", err)
		return err
	}

	return nil
}

func (svc *JobService) FetchApplicationStatus(applicationID string) (string, error) {
	resp, err := svc.api.GetByApplicationID(applicationID)
	if err != nil {
		return "", err
	}

	return resp.Status, nil
}
